﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleporterScript : MonoBehaviour
{
    [SerializeField] GameObject teleportHere;
    void OnTriggerEnter2D(Collider2D col)
    {
        col.gameObject.transform.position = new Vector3(teleportHere.transform.position.x, teleportHere.transform.position.y, 0);
    }
}
