﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButtonClicks : MonoBehaviour
{
    [SerializeField] string level1 = "grasslevel";
    [SerializeField] string level2 = "neonlevel";
    [SerializeField] string level3 = "lavalevel";
    [SerializeField] string level4 = "icelevel";

    [SerializeField] GameObject controlsMenu;
    [SerializeField] GameObject creditsMenu;
    public void PlayGame()
    {
        SceneManager.LoadScene("levelSelect");
    }
    public void CharacterSelect()
    {
        SceneManager.LoadScene("characterSelect");
    }

    public void PlayCredits()
    {
        creditsMenu.SetActive(true);
    }
    public void BackOutofCredits()
    {
        creditsMenu.SetActive(false);
    }
    public void ShowControls()
    {
        controlsMenu.SetActive(true);
    }
    public void BackOutofControls()
    {
        controlsMenu.SetActive(false);
    }
    public void QuitGame()
    {
        Application.Quit();
    }

    public void ChooseLevel1()
    {
        PlayerPrefs.SetString("level", level1);
    }
    public void ChooseLevel2()
    {
        PlayerPrefs.SetString("level", level2);
    }
    public void ChooseLevel3()
    {
        PlayerPrefs.SetString("level", level3);
    }
    public void ChooseLevel4()
    {
        PlayerPrefs.SetString("level", level4);
    }
}
