﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerScript : MonoBehaviour
{
    // Config
    [SerializeField] string horizontalString = "HorizontalP1";
    [SerializeField] string jumpString = "JumpP1";
    [SerializeField] string dashStringRight = "DashRightP1";
    [SerializeField] string dashStringLeft = "DashLeftP1";
    [SerializeField] string gPoundString = "GroundPoundP1";

    [SerializeField] string horizontalStringALT = "HorizontalP1ALT";
    [SerializeField] string jumpStringALT = "JumpP1ALT";
    [SerializeField] string dashStringRightALT = "DashRightP1ALT";
    [SerializeField] string dashStringLeftALT = "DashLeftP1ALT";
    [SerializeField] string gPoundStringALT = "GroundPoundP1ALT";

    [SerializeField] float runSpeed = 1.25f;
    [SerializeField] float jumpSpeed = 5;
    [SerializeField] float thrustGroundPound = 250;
    [SerializeField] float thrustDash = 1000;
    [SerializeField] float cooldownTime = 2;
    [SerializeField] float timeToWait = 0;

    [SerializeField] GameManager gameManager;

    // State
    public bool isAlive = true;
    bool isRunning = false;

    // Cached component references 
    Rigidbody2D myRigidBody;
    Animator myAnimator;
    CapsuleCollider2D myBodyCollider;
    BoxCollider2D myFeet;


    // Message then methods
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        myBodyCollider = GetComponent<CapsuleCollider2D>();
        myFeet = GetComponent<BoxCollider2D>();
        
    }

    // Update is called once per frame
    void Update()
    {
        //Die();
        if (isAlive)
        {
            Run();
            Jump();
            DashRight();
            DashLeft();
            GroundPound();
        }

        FlipSprite();
    }

    private void Run()
    {
        float controlThrow = CrossPlatformInputManager.GetAxis(horizontalString) + CrossPlatformInputManager.GetAxis(horizontalStringALT);// value between -1 & 1
        Vector2 playerVelocity = new Vector2(controlThrow * runSpeed, myRigidBody.velocity.y);
        myRigidBody.velocity = playerVelocity;

        bool playerHasHorizontalSpeed = Mathf.Abs(myRigidBody.velocity.x) > Mathf.Epsilon;
        myAnimator.SetBool("isRunning", playerHasHorizontalSpeed);

    }

    private void Jump()
    {
        if (!myFeet.IsTouchingLayers(LayerMask.GetMask("Ground", "Player"))) { myAnimator.SetBool("isJumping", false); return; }

        if (CrossPlatformInputManager.GetButtonDown(jumpString))
        {
            print(jumpString);
            Vector2 jumpVelocityToAdd = new Vector2(0f, jumpSpeed);
            myRigidBody.velocity += jumpVelocityToAdd;

            myAnimator.SetBool("isJumping", true);
        }

        if (CrossPlatformInputManager.GetButtonDown(jumpStringALT))
        {
            print(jumpStringALT);
            Vector2 jumpVelocityToAdd = new Vector2(0f, jumpSpeed);
            myRigidBody.velocity += jumpVelocityToAdd;

            myAnimator.SetBool("isJumping", true);
        }
    }
    private void DashRight()
    {
        if(Time.time > timeToWait)
        {
            if (CrossPlatformInputManager.GetButtonDown(dashStringRight))
            {
                print("dashed right");
                myRigidBody.AddForce(this.transform.right * thrustDash);

                timeToWait = Time.time + cooldownTime;
            }
            if (CrossPlatformInputManager.GetButtonDown(dashStringRightALT))
            {
                print("dashed right");
                myRigidBody.AddForce(this.transform.right * thrustDash);

                timeToWait = Time.time + cooldownTime;
            }
        }

    }
    private void DashLeft()
    {
        if (Time.time > timeToWait)
        {
            if (CrossPlatformInputManager.GetButtonDown(dashStringLeft))
            {
                print("dashed left");
                myRigidBody.AddForce(this.transform.right * -thrustDash);

                timeToWait = Time.time + cooldownTime;
            }
            if (CrossPlatformInputManager.GetButtonDown(dashStringLeftALT))
            {
                print("dashed left");
                myRigidBody.AddForce(this.transform.right * -thrustDash);

                timeToWait = Time.time + cooldownTime;
            }
        }
    }

    private void GroundPound()
    {
        if (CrossPlatformInputManager.GetButtonDown(gPoundString))
        {
            print("pounded");
            myRigidBody.AddForce(this.transform.up * -thrustGroundPound);
        }
        if (CrossPlatformInputManager.GetButtonDown(gPoundStringALT))
        {
            print("pounded");
            myRigidBody.AddForce(this.transform.up * -thrustGroundPound);
        }
    }
    public void KillHit(Rigidbody2D rb)
    {
        Debug.Log("Killhit called");
        PlayerScript enemy = rb.gameObject.GetComponent<PlayerScript>() ;
        if(enemy != null)
        {
        enemy.Die();
        }


    }
    public void Die()
    {
        Debug.Log("Die called");
        isAlive = false;
        this.gameObject.SetActive(false);
        //Destroy(gameObject);
        gameManager.OnDeath();
    }
//    private void OnDestroy()
//    {

//    }
    private void FlipSprite()
    {
        bool playerHasHorizontalSpeed = Mathf.Abs(myRigidBody.velocity.x) > Mathf.Epsilon;
        if (playerHasHorizontalSpeed)
        {
            transform.localScale = new Vector2(Mathf.Sign(myRigidBody.velocity.x), 1f);
        }
    }
}
