﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillOutsideScreen : MonoBehaviour
{

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.attachedRigidbody == null) return;

        PlayerScript enemy = col.gameObject.GetComponent<PlayerScript>();
        if (enemy != null)
        {
            enemy.Die();
        }
    }

}
