﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] int playersToKill = 3;
    [SerializeField] CharacterSelection player1;
    [SerializeField] CharacterSelection player2;
    [SerializeField] CharacterSelection player3;
    [SerializeField] CharacterSelection player4;

    public int p1Score = 0;
    public int p2Score = 0;
    public int p3Score = 0;
    public int p4Score = 0;

    //text from ui
    [SerializeField] Text p1Text;
    [SerializeField] Text p2Text;
    [SerializeField] Text p3Text;
    [SerializeField] Text p4Text;
    [SerializeField] Text roundsText;
    [SerializeField] Text instructionsText;
    int roundNumber = 1;
    [SerializeField] GameObject roundScreen;

    //customize... for designer an hopefully winning score also for players
   //int WinningScore = 3;
    [SerializeField] int pauseTime = 3;
    int storePlayersToKill = 3;

    bool isWinner = false;

    // Start is called before the first frame update
    public void Start()
    {
        storePlayersToKill = playersToKill;
    }
    public void OnDeath()
    {
        playersToKill -= 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (playersToKill <= 0) // if end of round basically
        {
            playersToKill = storePlayersToKill;
            AssignPoints();
            CheckWinner();
            StartCoroutine(ResetRound());
            //SceneManager.LoadScene(0);

        }
    }

    void AssignPoints()
    {
        //enable round over screen
        roundScreen.SetActive(true);
        //check whose alive?
                //am doing this in character selection
        //give appropriate points 
        if (player1.IsAlive())
        {
            p1Score++;
        }
        if (player2.IsAlive())
        {
            p2Score++;
        }
        if (player3.IsAlive())
        {
            p3Score++;
        }
        if (player4.IsAlive())
        {
            p4Score++;
        }
        //update UI to reflect scores
        p1Text.text = "P1: " + p1Score;
        p2Text.text = "P2: " + p2Score;
        p3Text.text = "P3: " + p3Score;
        p4Text.text = "P4: " + p4Score;
        //update round number... 
        roundNumber++;
        roundsText.text = "Round " + roundNumber + "!";
        instructionsText.text = "First to " + PlayerPrefs.GetInt("WinningScore") + ", WINS adoption!";

    }

    IEnumerator ResetRound()
    {
        yield return new WaitForSeconds(pauseTime);
        if (isWinner)
        {
            yield return new WaitForSeconds(pauseTime);

            SceneManager.LoadScene("menu");
        }
        roundScreen.SetActive(false);
        //reactivate players
        player1.RespawnPlayer();
        player2.RespawnPlayer();
        player3.RespawnPlayer();
        player4.RespawnPlayer();

        Debug.Log("Reseting round...");
    }


    void CheckWinner()
    {
        if (p1Score >= PlayerPrefs.GetInt("WinningScore"))
        {
            isWinner = true;
            roundsText.text = "Player 1";
            instructionsText.text = "IS THE WINNER!";
        }    
        else if(p2Score >= PlayerPrefs.GetInt("WinningScore"))
        {
            isWinner = true;
            roundsText.text = "Player 2";
            instructionsText.text = "IS THE WINNER!";
        }
        else if(p3Score >= PlayerPrefs.GetInt("WinningScore"))
        {
            isWinner = true;
            roundsText.text = "Player 3";
            instructionsText.text = "IS THE WINNER!";
        }
        else if(p4Score >= PlayerPrefs.GetInt("WinningScore"))
        {
            isWinner = true;
            roundsText.text = "Player 4";
            instructionsText.text = "IS THE WINNER!";
        }
    }

}
