﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathFeet : MonoBehaviour {

    public PlayerScript myPlayer;

	void OnTriggerEnter2D(Collider2D col)
    {
        Debug.Log("collider death feet triggered");
        if (col.attachedRigidbody == null)  return; 
            

          myPlayer.KillHit(col.attachedRigidbody);
    }

}
