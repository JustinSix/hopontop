﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChooseRoundsToWinSlider : MonoBehaviour
{
    Text displayedText;
    [SerializeField] Slider onlySlider;
    private void Start()
    {
        displayedText = GetComponent<Text>();
        displayedText.text = PlayerPrefs.GetInt("WinningScore", 3).ToString();
    }
    // Update is called once per frame
    public void textUpdate()
    {
        displayedText.text = onlySlider.value.ToString();

        PlayerPrefs.SetInt("WinningScore", (int)onlySlider.value);
    }
}
