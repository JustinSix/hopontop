﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterSelection : MonoBehaviour
{
    private GameObject[] characterList;

    private int index;

    [SerializeField] string playerPref = "CharacterSelected1";

    //references to spawn points
    [SerializeField] GameObject SpawnPoint;

    private void Start()
    {
        index = PlayerPrefs.GetInt(playerPref);

        characterList = new GameObject[transform.childCount];

        //Fill the array with our models
        for (int i = 0; i < transform.childCount; i++)
        {
            characterList[i] = transform.GetChild(i).gameObject;
        }
        //We toggle off their renderer
        foreach(GameObject go in characterList)
        {
            go.SetActive(false);
        }

        //We toggle on the selected character
        if(characterList[index])
        {
            characterList[index].SetActive(true);
        }

    }

    public void ToggleLeft()
    {
        //Toggle off the current model
        characterList[index].SetActive(false);

        index--; // index -= 1;
        if(index < 0)
        {
            index = characterList.Length - 1;
        }

        //Toggle on the new model
        characterList[index].SetActive(true);
    }

    public void ToggleRight()
    {
        //Toggle off the current model
        characterList[index].SetActive(false);

        index++; // index += 1;
        if (index == characterList.Length)
        {
            index = 0;
        }

        //Toggle on the new model
        characterList[index].SetActive(true);
    }

    public void ConfirmButton()
    {
        PlayerPrefs.SetInt(playerPref, index);
    }

    public void RespawnPlayer()
    {
        Debug.Log("Respawning player...");
        //move to spawn point
        Vector3 spawnPosition = SpawnPoint.transform.position;
        characterList[index].transform.position = spawnPosition;
        //renable player and controls
        characterList[index].SetActive(true);
        characterList[index].GetComponent<PlayerScript>().isAlive = true;



    }
    public bool IsAlive()
    {
        if (characterList[index].activeSelf)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
